﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Plugin.Widgets.GformAdd.Model;
using Nop.Services.Caching;
using Nop.Services.Configuration;
using Nop.Services.Media;
using Nop.Web.Framework.Components;

namespace Nop.Plugin.Widgets.GformAdd.Components
{
    [ViewComponent(Name = "WidgetsGformAdd")]
    public class WidgetsGformAddComponent  : NopViewComponent
    {
        private readonly ICacheKeyService _cacheKeyService;
        private readonly IStoreContext _storeContext;
        private readonly IStaticCacheManager _staticCacheManager;
        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;

        public WidgetsGformAddComponent(ICacheKeyService cacheKeyService, IStoreContext storeContext, IStaticCacheManager staticCacheManager, ISettingService settingService, IWebHelper webHelper)
        {
            _cacheKeyService = cacheKeyService;
            _storeContext = storeContext;
            _staticCacheManager = staticCacheManager;
            _settingService = settingService;
            _webHelper = webHelper;
        }

        public IViewComponentResult Invoke(string widgetZone, object additionalData)
        {
            var gformAddSetting = _settingService.LoadSetting<GformAddSetting>(_storeContext.CurrentStore.Id);
            var model = new PublicInfoModel
            {
                FormLink = gformAddSetting.GformLink,
            };
            if (string.IsNullOrEmpty(model.FormLink)) return Content("");


            return View("~/Plugins/Widgets.GformAdd/View/PublicInfo.cshtml", model);
        }


    }
}
