﻿using System;
using System.Collections.Generic;
using System.Text;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Plugin.Widgets.GformAdd.Model
{
    public class ConfigarationModel : BaseNopModel
    {
        public int ActiveStoreScopeConfiguration { get; set; }
        [NopResourceDisplayName("Plugins.Widgets.GformAdd.Link")]
        public string Link { get; set; }
        public bool Link_OverrideForStore { get; set; }

    }

}
