﻿using System;
using System.Collections.Generic;
using System.Text;
using Nop.Web.Framework.Models;

namespace Nop.Plugin.Widgets.GformAdd.Model
{
    public class PublicInfoModel : BaseNopModel
    {
        public string FormLink { get; set; }
    }
}
