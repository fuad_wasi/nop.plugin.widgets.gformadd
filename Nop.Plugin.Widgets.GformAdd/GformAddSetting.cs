﻿using System;
using System.Collections.Generic;
using System.Text;
using Nop.Core.Configuration;

namespace Nop.Plugin.Widgets.GformAdd
{
    public class GformAddSetting : ISettings
    {
        public string  GformLink { get; set; }
        public int LinkId { get; set; }
    }
}
