﻿using System;
using System.Collections.Generic;
using System.Text;
using Nop.Core;
using Nop.Core.Infrastructure;
using Nop.Services.Cms;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Plugins;
using Nop.Web.Framework.Infrastructure;

namespace Nop.Plugin.Widgets.GformAdd
{
    public class GformAddPlugin : BasePlugin, IWidgetPlugin
    {
        private readonly ILocalizationService _localizationService;
        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;

        public GformAddPlugin(ILocalizationService localizationService, ISettingService settingService, IWebHelper webHelper)
        {
            _localizationService = localizationService;
            _settingService = settingService;
            _webHelper = webHelper;
        }




        //public bool HideInWidgetList => throw new NotImplementedException();

        public string GetWidgetViewComponentName(string widgetZone)
        {
            return "WidgetsGformAdd";
        }

        public IList<string> GetWidgetZones()
        {
            //var a = PublicWidgetZones.ToL;
            return new List<string> { PublicWidgetZones.HomepageBeforeNews };
        }

        public override string GetConfigurationPageUrl()
        {
            return _webHelper.GetStoreLocation() + "Admin/WidgetsGformAdd/Configure";
        }

        /// <summary>
        /// Install plugin
        /// </summary>
        public override void Install()
        {
            //pictures
            

            //settings
            var settings = new GformAddSetting
            {
                GformLink = "",
            };
            _settingService.SaveSetting(settings);

            _localizationService.AddPluginLocaleResource(new Dictionary<string, string>
            {
                ["Plugins.Widgets.GformAdd.Link"] = "Link",
            });

            base.Install();
        }
        public override void Uninstall()
        {
            //settings
            _settingService.DeleteSetting<GformAddSetting>();

            //locales
            //_localizationService.DeletePluginLocaleResources("Plugins.Widgets.NivoSlider");

            base.Uninstall();
        }
        public bool HideInWidgetList => false;
    }
}
